function [tgamma, tau] = penaltyfactor(N, inputs, counts)
% PENALTYFACTOR Compute the penalty factor tau^(gamma(x)/n)
%    [pen, tau] = PENALTYFACTOR(N, inputs, counts)

    global_flag = inputs(1) && inputs(2);
    tau = 2*(global_flag + 1);

    joint_freqs = bsxfun(@rdivide,counts,N);

    ndx = inputs2ind(inputs);
    xr_freqs = sum(joint_freqs(ndx,:));
    xnotr_freqs = 1 - xr_freqs;

    tgamma = tau.^xnotr_freqs;
end
