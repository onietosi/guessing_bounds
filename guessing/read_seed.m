function [found] = read_seed(directory)
% READ_SEED     Initialize RNG with seed.cfg or shuffle if it doesn't exist
%    FOUND = READ_SEED(D) looks for seed.cfg in directory D.

fname = [directory '/seed.cfg'];
if exist(fname, 'file')
    found = 1;
    seed = dlmread(fname);
    rng(seed);
else
    found = 0;
    fprintf(1, 'Seed file seed.cfg not found, shuffling RNG instead.\n');
    rng('shuffle');
end

end
