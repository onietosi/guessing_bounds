function [ ndx ] = inputs2ind(inputs)
%INPUT2IND Gives index components in p(ab|xy) with inputs [x1,y1;x2,y2...]

if all(all(inputs))
% Put inputs x and y in column vector
x = inputs(:,1);
y = inputs(:,2);

% For each pair of inputs output first index in p(ab|xy) with inputs (x,y)
ndx = 1 + 8*(x-1) + 4*(y-1);

% We are interested in the 4 components (a,b) for a given (x,y). Calculate
% the correponding indices and matrix with a row for each (x,y)
ndx = bsxfun(@plus,ndx,0:3);

% Put all indices in column vector
ndx = ndx(:);

else
    warning(['You are trying to compute a local guessing probability. ',...
        'This function is not adapted to that case.'])

end

