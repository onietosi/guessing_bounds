function T = prob2ev_ns(p_XY)
% PROB2EV.m     Constructs T such that ev=T*p
%               ev is a 9-component column-vector containing the 
%               expectation values <I>, <A_i>, <B_j>, <A_i x B_j>,
%               and p is a 16-component column-vector containing the 
%               probabilities P(ab|xy). Takes as argument the input
%               probabilities P_XY(xy)
% Explicitly:
% ev' = (<I> <A1> <A2> <B1> <B2> <A1xB1> <A1xB2> <A2xB1> <A2xB2>)
% where <I>= sum_{a,b,i,j} P(ab|ij) P_{XY}(ij)
%       <A_i>= sum_{a,b,j} a*P(ab|ij) P_{Y|X}(j|i)
%       <B_j>= sum_{a,b,i} b*P(ab|ij) P_{X|Y}(i|j)
%       <A_i x B_j>= sum_{a,b} a*b*P(ab|ij)
%
% p = [ P(1,1|1,1) P(1,-1|1,1) P(-1,1|1,1) P(-1,-1|1,1) P(1,1|1,2) ... 
% P(-1,-1|1,2) P(1,1|2,1) ... P(-1,-1|2,2) ]
%
% P_XY = [ P_XY(1,1) P_XY(1,2) P_XY(2,1) P_XY(2,2) ]

if ~exist('p_XY', 'var')
    warning('no input argument given to prob2ev_ns, falling back to uniform inputs');
    p_XY = [1 1 1 1]/4;
end

if numel(p_XY) == 16
    %p_XY has gone through repmat and is redundant; extract the four weights
    p_XY = p_XY(1:4:16);
end

% If the input is a 4x4 matrix, convert to column-vector 
% p=matrix2vec(p);

% Build M as a sparse matrix
i_index=[ones(1,16) 2*ones(1,8) 3*ones(1,8) 4*ones(1,8) 5*ones(1,8)...
    6*ones(1,4) 7*ones(1,4) 8*ones(1,4) 9*ones(1,4) ];
j_index=[1:16 1:8 9:16 1:4 9:12 5:8 13:16 1:4 5:8 9:12 13:16];

norm=ones(1,4); % these parameters are arbitrarily chosen!
corr=[1 -1 -1 1];
marg_A=[1 1 -1 -1];
marg_B=[1 -1 1 -1];

p_XcY1 = p_XY([1,3])/(p_XY(1)+p_XY(3));  %p(X|Y=1)
p_XcY2 = p_XY([2,4])/(p_XY(2)+p_XY(4));  %p(X|Y=2)
p_YcX1 = p_XY([1,2])/(p_XY(1)+p_XY(2));  %p(Y|X=1)
p_YcX2 = p_XY([3,4])/(p_XY(3)+p_XY(4));  %p(Y|X=2)

ij_value=[ p_XY(1)*norm      p_XY(2)*norm      p_XY(3)*norm      p_XY(4)*norm      ...
           p_YcX1(1)*marg_A  p_YcX1(2)*marg_A                                      ...
                                               p_YcX2(1)*marg_A  p_YcX2(2)*marg_A  ...
           p_XcY1(1)*marg_B                    p_XcY1(2)*marg_B                    ...
                             p_XcY2(1)*marg_B                    p_XcY2(1)*marg_B  ...
           corr                                                                    ...
                             corr                                                  ...
                                               corr                                ...
                                                                 corr              ];

T = sparse(i_index,j_index,ij_value);

end