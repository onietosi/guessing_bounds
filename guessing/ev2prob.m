function S = ev2prob
% EV2PROB Constructs (sparse) matrix that transforms from correlator to
% probability representation, using the following relation:
% P(ab|xy)=1/4(1+a<A_x>+b<B_y>+ab<A_x B_y>)
i_index=[1:16 1:16 1:16 1:16];
j_index=[ones(1,16) 2*ones(1,8) 3*ones(1,8) 4*ones(1,4) 5*ones(1,4)...
    4*ones(1,4) 5*ones(1,4) 6*ones(1,4) 7*ones(1,4) 8*ones(1,4) 9*ones(1,4)];

marg_A=[1 1 -1 -1];
marg_B=[1 -1 1 -1];
corr=[1 -1 -1 1];

ij_value=[ones(1,16) marg_A marg_A marg_A marg_A marg_B marg_B marg_B marg_B...
    corr corr corr corr];

S = 1/4*sparse(i_index,j_index,ij_value);

end