function g_vs_N(filepath)
% G_VS_N        Calculates different guessing bounds from Monte-Carlo
%               generated data for different number of rounds N
%
% G_VS_N(DIR) gets its configuration files from directory DIR.
%
% Variables written to the output files (csv and/or mat):
%
%  N            : number of rounds
%  gammalogt    : gamma * log2(tau), logarithm of the penalty factor in the
%                 guessing probability
%
% Min-entropies computed with respect to different (sets of) inequalities.
%
%  h_chsh       : CHSH, all inputs (no penalty)
%  h_th_chsh    : CHSH, all inputs, asymptotic value
%  h_th         : p, one input, asymptotic value
%  h_th_all     : p, all inputs, asymptotic value
%  h_corr2      : 4 correlators + 4 CHSH, one input, no penalty
%  ht_corr2     : 4 correlators + 4 CHSH, one input, penalty
%  h_corr2_all  : 4 correlators + 4 CHSH, all inputs (no penalty)
%
% Not present in the csv file:
%  rng_state    : RNG state
%  c_ev_opt     : optimal inequality for GP of underlying model, one input
%  c_ev_opt_all : optimal inequality for GP of underlying model, all inputs


%% RETRIEVE DATA FROM DATAFILES
disp '>>> importing data'

% Initialise seed of random number generator
read_seed(filepath);
% We will save the full rng state into the .mat file
data_extra.rng_state = rng;

% Get protocol parameters (number of rounds, correlations to be simulated...)
cfg = read_configuration(filepath);

[freqs,p_XY,counts] = importMC(cfg);

N            = cfg.N;
inputs       = cfg.inputs;
theta        = cfg.theta;
prob         = cfg.prob;

data.N = N;

% DEFINE SECURITY PARAMETER
epsi = 1E-6;

% Full set of inputs
inputs_all = [1 1; 1 2; 2 1; 2 2];

%% CALCULATE ERROR TERM tau^(gamma(x)/n)
tgamma = penaltyfactor(N, inputs, counts);
data.gammalogt = log2(tgamma);


%% CHSH GUESSING BOUND
disp '>>> bound_chsh'
[g_chsh,~,nu_chsh] = bound_chsh(inputs,freqs,p_XY,N,epsi);

data.h_chsh = -log2(g_chsh);
data_extra.nu_chsh = nu_chsh;


%% CALCULATE OPTIMAL EXPRESSIONS
c_ev = [zeros(8,1) eye(8)];
T = prob2ev_ns([1 1 1 1]/4); %weights don't matter for quantum points
ev = c_ev*T*prob;

% FIXME: duplicate code
disp '>>> theoretical guessing probability'

disp '>>>> one input'
[pguess,c_ev_opt] = guessing_probability(c_ev,ev,inputs);
c_ev_opt = reshape(c_ev_opt, 1, 9); %guessing_probability returns a column vector, we use rows
data.h_th = -log2(pguess) * ones(1,length(N));
data_extra.c_ev_opt = c_ev_opt;

disp '>>>> all inputs'
[pguess_all,c_ev_opt_all] = guessing_probability(c_ev,ev,inputs_all);
c_ev_opt_all = reshape(c_ev_opt_all, 1, 9);
data.h_th_all = -log2(pguess_all) * ones(1,length(N));
data_extra.c_ev_opt_all = c_ev_opt_all;

%% CHSH-CORRELATORS GUESSING BOUND
disp '>>> bound_correlators_chsh'
epsi_up2 = epsi/16*ones(8,1);
epsi_low2 = epsi/16*ones(8,1);
[g_corr2,~,nu_corr2] = bound_correlators_chsh(inputs,freqs,p_XY,N,[epsi_up2,epsi_low2]);
gt_corr2 = min(g_corr2.*tgamma,1);

data.h_corr2 = -log2(g_corr2);
data.ht_corr2 = -log2(gt_corr2);
data_extra.nu_corr2 = nu_corr2;

%% CHSH-CORRELATORS GUESSING BOUND, ALL INPUTS
disp '>>> bound_correlators_chsh_all'
g_corr2_all = bound_correlators_chsh(inputs_all,freqs,p_XY,N,[epsi_up2,epsi_low2]); %same epsilons

data.h_corr2_all = -log2(g_corr2_all);

%% CHSH GUESSING PROBABILITY
disp '>>> chsh guessing probability'

f_chsh = [ 0 0 0 0 0 1 1 1 -1];
T = prob2ev_ns([1 1 1 1]/4);
ev_chsh = f_chsh*T*prob;
[pguess_chsh,y_chsh] = guessing_probability(f_chsh,ev_chsh,inputs);
pguess_chsh = pguess_chsh*ones(1,length(N));

data.h_th_chsh = -log2(pguess_chsh);

%% SAVE DATA
disp '>>> writing data'

writedata([filepath '/hvsN'], data, data_extra);

end

function writedata(base, data, data_extra)
	writedlm([base, '.csv'], data);
	save([base, '.mat'], '-struct', 'data');
	save([base, '.mat'], '-struct', 'data_extra', '-append');
end

function s = datastruct(varargin)
	for k = 1:nargin
		s.(inputname(k)) = varargin{k};
	end
end
