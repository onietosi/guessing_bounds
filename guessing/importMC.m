function [freqs,p_XY,counts] = importMC(cfg)

% Declare arrays to store data used in calculations
numN = numel(cfg.N);
freqs = zeros(16,numN);
p_XY = zeros(16,numN);
counts = zeros(16,numN);

for k = 1:numN
    [pxy_tmp, freqs(:,k), counts(:,k)] = ...
        MCXP_fast( cfg.N(k), cfg.inputs, cfg.theta, cfg.bias_delta, ...
                   cfg.bias_gamma,  cfg.prob, cfg.v );
    % Take input probabilities p_XY=[a,b,c,d] for given N and put them in
    % column vector p_XY(:,k)=[a;a;a;a;b;b;b;b;c;c;c;c;d;d;d;d]
    p_XY(:,k)= reshape(repmat(pxy_tmp,4,1),16,1);
end

end
