function [ev,extra] = correlations(corr,cfg)
% CORRELATIONS  Define bipartite, 2 input, 2 output (quantum) correlations
%
% CORR can be those maximally violating the CHSH inequality ('noisy_singlet')
% or those maximally violating the I_alpha_beta inequalities (with alpha=1)
% THETA defines the degree of entanglement of the qstate
% V determines the visibility (v=1 for noiseless behaviour)

% Correlators are first arranged in matrix
%  1   B1    B2
%  A1  A1B1  A1B2
%  A2  A2B1  A2B2
% and then rearranged and output as column vector
% (1,A1,A2,B1,B2,A1B1,A1B2,A2B1,A2B2)

% White noise correlation table
ev_mixed=zeros(3,3);
ev_mixed(1,1)=1;

switch corr
    case 'noisy_singlet'
        v = cfg.v;
        ev_chsh=[1 0 0;...
                0 sqrt(2)/2 sqrt(2)/2;...
                0 sqrt(2)/2 -sqrt(2)/2];

        ev = v*ev_chsh + (1-v)*ev_mixed;
        
    case 'I_alpha_beta'
        % Correlations maximally violating I_1^beta for degree of entanglement theta
        theta = cfg.theta;
        v = cfg.v;
        alpha=1;
        beta = 2*cos(2*theta)/sqrt(1+(sin(2*theta))^2);
        mu=atan(sin(2*theta)/alpha);
        
        ev1=[       1       cos(mu)*cos(2*theta)    cos(mu)*cos(2*theta);
             cos(2*theta)       cos(mu)                 cos(mu);
                    0       sin(2*theta)*sin(mu)    -sin(2*theta)*sin(mu)];
        
        ev=v*ev1+(1-v)*ev_mixed;
end

ev=[ev(1,1) ev(2,1) ev(3,1) ev(1,2) ev(1,3) ev(2,2) ev(2,3) ev(3,2) ev(3,3)];
ev=ev';

end
