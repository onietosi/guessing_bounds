function [outcfg] = read_configuration(directory)
% READ_CONFIGURATION     Reads configuration.m into current workspace
%     STRUCT = READ_CONFIGURATION(D) looks for configuration.m in directories
%     specified in the cell array D. The first file found is executed, and the
%     contents of the resulting 'cfg' structure are returned.

found = 0;

if ~iscell(directory)
	directory = {directory};
end

for i = 1:numel(directory)
	fname = [directory{i} '/configuration.m'];
	if exist(fname, 'file')
		found = 1;
		break;
	end
end

if ~found
	outcfg = 0;
	error('Could not find configuration.m.');
else
	run(fname);
	outcfg = cfg;

	% GET CORRELATIONS AND COEFFICIENTS (correlator representation)
	behaviour_ev = correlations(cfg.correlations, cfg);
	% Convert to probability representation. Matrix S converts correlators
	% to probabilities.
	S=ev2prob;
	outcfg.prob=S*behaviour_ev;

end

end
