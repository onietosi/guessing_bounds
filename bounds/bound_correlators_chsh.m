function [bound,y,nu] = bound_correlators_chsh(inputs,freqs,p_XY,N,epsi)
% BOUND_CORRELATORS_CHSH    Calculate guessing bound given estimate of 
%                           local correlators (<A_x>,<B_y>) and four
%                           independent CHSH expressions, for a range of N

epsi_up = epsi(:,1);
epsi_low = epsi(:,2);

% Define matrix of coefficients in correlator representation (each line
% corresponds to a Bell expression; in this case, each line represents a
% correlator)
c_ev = [   0  1  0  0  0  0  0  0  0;...
           0  0  1  0  0  0  0  0  0;...
           0  0  0  1  0  0  0  0  0;...
           0  0  0  0  1  0  0  0  0;...
           0  0  0  0  0  1  1  1 -1;...
           0  0  0  0  0  1  1 -1  1;...
           0  0  0  0  0  1 -1  1  1;...
           0  0  0  0  0 -1  1  1  1];

% Estimate confidence region
I_xp = zeros(8, length(N));
% Prefactor nu_alpha
nu = zeros(8,length(N));
for k=1:length(N)
    % Transform coefficients to probability representation
    T = prob2ev_ns(p_XY(:,k));
    c_p = c_ev*T;
    I_xp(:,k) = c_p * freqs(:,k);

    nu(1:4,k) = max(abs(bsxfun(@rdivide,c_p(1:4,:)',p_XY(:,k))))+1;
end
nu(5:8,:) = repmat(1./min(p_XY)+2*sqrt(2),4,1);
% Square root term
sqrt_up = bsxfun(@times,sqrt(log(1./epsi_up)),sqrt(2./N));
sqrt_low = bsxfun(@times,sqrt(log(1./epsi_low)),sqrt(2./N));

mu_up = bsxfun(@times,nu,sqrt_up);
mu_low = bsxfun(@times,nu,sqrt_low);

confreg_up = I_xp + mu_up;
confreg_low = I_xp - mu_low;

% Calculate guessing bound
yalmip clear
[bound,y] = guessing_bound(inputs,c_ev,confreg_up,confreg_low);
end
