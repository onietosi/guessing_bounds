function [bound,y,nu] = bound_chsh(inputs,freqs,p_XY,N,epsi)
% BOUND_CHSH    Calculate guessing bound given estimate of I_CHSH for a 
%               range of N

% Define coefficients in correlator representation
c_ev=[0 0 0 0 0 1 1 1 -1];
% Transform coefficients to probability representation
T = prob2ev_ns([1 1 1 1]/4); %we only use full correlators, input distribution doesn't matter here
c_p=c_ev*T;
% Define maximal violation of CHSH expression
I_max = 2*sqrt(2);

% Estimate Bell violation and confidence interval
I_xp = c_p*freqs;

mu_sqrt = sqrt(2./N*log(1/epsi));
mu = (1./min(p_XY)+I_max).*mu_sqrt;

% Calculate guessing bound
yalmip clear
[bound,y] = guessing_bound(inputs,-c_ev,-I_xp+mu);
end

