function [bound,y] = guessing_bound(inputs,bell_coeffs,varargin)
% GUESSING_BOUND    Calculate guessing bound over confidence region
%
% Calculate guessing bound over a confidence region around t estimated Bell
% expression(s).
%
% INPUTS determines the input settings for which the bound is to be
% calculated.
%
% BELL_COEFFS is a (t x 9) matrix containing the t Bell expressions in the
% correlator representation.
%
% VARARGIN contains the confidence region for the t expressions. If
% VARARGIN is tx1, then the confidence region consists of t one-sided
% confidence intervals. If VARARGIN is tx2, then the confidence region
% consists of t two-sided intervals.

% Check number of arguments
narginchk(3,4);
% Calculate number of inputs
n_r = size(inputs,1);
% Calculate number of Bell expressions t
n_bell = size(bell_coeffs,1);
% Check if confidence region is one-sided or two sided
nvarargin = length(varargin);

yalmip clear
constraints = [];

% One-sided confidence interval(s)
if nvarargin==1
    % Define confidence interval(s) boundaries
    confreg = varargin{1};
    
    % Declare optimisation variables
    y0 = sdpvar(1);
    y = sdpvar(1,n_bell);
    
    % Declare confidence interval(s) boundaries as parameters (passed to
    % optimizer below)
    confreg_par = sdpvar(n_bell,1);
    
    %%% Define constraints
    % SOS constraints
    for i=1:n_r
        constraints = constraints +...
            define_constraints(y0*eye(1,9) + y*bell_coeffs,inputs(i,:));
    end
    % Positivity constraints of the optimisation variables
    constraints = constraints + [y >= 0];
    
    % Declare objective function
    obj = y0 + y*confreg_par;
    
    % Define optimizer and solve problem
    opt = optimizer(constraints,obj,sdpsettings('solver','sedumi'),...
        confreg_par,{obj,[y0;y(:)]});
    soln = opt{confreg};
    
    % Output solution
    bound = soln{1};
    y = soln{2};    
    
elseif nvarargin==2
    % Define confidence region boundaries
    confreg_up = varargin{1};
    confreg_low = varargin{2};    
    
    % Declare optimisation variables
    y0 = sdpvar(1);
    y_up = sdpvar(1,n_bell);
    y_low = sdpvar(1,n_bell);
    
    % Declare confidence region boundaries as parameters (passed to
    % optimizer below)
    confreg_low_par = sdpvar(n_bell,1);
    confreg_up_par = sdpvar(n_bell,1);
    
    %%% Define constraints
    % SOS constraints
    for i=1:n_r
        constraints = constraints +...
            define_constraints(y0*eye(1,9) + (y_up-y_low)*bell_coeffs,inputs(i,:));
    end
    % Positivity constraints of the optimisation variables    
    constraints = constraints + [y_up >= 0, y_low >=0];
    
    % Declare objective function
    obj = y0 + y_up*confreg_up_par - y_low*confreg_low_par;
    
    % Define optimizer and solve problem
    opt = optimizer(constraints,obj,sdpsettings('solver','sedumi'),...
        [confreg_up_par;confreg_low_par],{obj,[y0;y_up(:);y_low(:)]});
    soln = opt{[confreg_up;confreg_low]};
    
    % Output solution
    bound = soln{1};
    y = soln{2};    
end
end