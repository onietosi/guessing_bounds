% CONFIGURATION     Defines parameters of experiment and protocol

% Number of rounds
cfg.N = reshape(bsxfun(@times, [1;3], 10.^[2:18]), 1, []);

% Parameters determining underlying device behaviour
cfg.correlations='I_alpha_beta';
cfg.v=0.99; % visibility
cfg.theta=pi/8; % degree of entanglement

cfg.inputs=[2,1]; % Randomness generating inputs
cfg.bias_delta=1/2; % Bias parameters;
cfg.bias_gamma=1/5; % input distribution takes form p_r=1-bias_gamma*N^(-gamma)
