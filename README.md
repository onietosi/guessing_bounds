# Bounding the guessing probability in Bell experiments

This script extends our [methods](https://gitlab.com/onietosi/guessing_probability) to experimental settings, in which the guessing probability cannot be exactly calculated and one must resort to bounds on this quantity.

**Repository structure**:

- **guessing**: folder containing the main script `g_vs_N.m`, that outputs bounds on the guessing probability for a range of $N$ (the number of rounds of the experiment);
- **bounds**: folder containing the functions to calculate the bounds on the guessing probability;
- **monte_carlo**: folder containing the function `MCXP_fast.m`, that generates the pseudo-experimental data through Monte-Carlo sampling.

**Requirements**:

- [guessing probability script](https://gitlab.com/onietosi/guessing_probability)
- [YALMIP](https://yalmip.github.io/)
- [SeDuMi](http://sedumi.ie.lehigh.edu/)

**Authors**: Olmo Nieto Silleras and Cédric Bamps.

## References

O. Nieto-Silleras, C. Bamps, J. Silman, & S. Pironio, Device-independent randomness generation from several Bell estimators, [_New Journal of Physics_ **20**, 023049](https://doi.org/10.1088/1367-2630/aaaa06), 2018.

See also **chapter 6** of my [PhD thesis](https://dipot.ulb.ac.be/dspace/bitstream/2013/271365/3/ONietoSilleras-thesis.pdf).