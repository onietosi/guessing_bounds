function [p_XY, freq, counts] = MCXP_fast(N, inputs, theta, bias_delta, bias_gamma, prob, v)
% MCXP_FAST Simulate the experiment given protocol parameters.
%    [P_XY,F,C] = MCXP_fast(...) returns the input probabilities, frequencies
%    and counts from one run of the protocol given parameters.
%
%    The parameters are the following:
%     N            - Number of rounds
%     inputs       - Randomness-generating inputs
%     theta        - Tilting parameter (entanglement angle)
%     bias_delta   - Input distribution bias factor
%     bias_gamma   - Input distribution bias exponent
%     prob         - Sampled probabilities (see ev2prob function)
%     v            - Visibility in simulated box



    % DEFINE BIASED INPUT DISTRIBUTION (assumes dichotomic measurement
    % settings)
    x_r=inputs(1);
    y_r=inputs(2);

    p_notr=min(3*bias_delta*N^(-bias_gamma),3/4);
    p_r=1-p_notr;

    % We take all 'non-random' elements of input distribution to be uniformly
    % distributed (in order to have a balanced sample and enough counts for
    % all inputs).
    % p_XY={P(x=o,y=o),P(x=o,y=1);P(x=1,y=0),P(x=1,y=1)}
    p_XY=p_notr/3*ones(2); 
    p_XY(x_r,y_r) = p_r;
    % Arrange as row vector (order as above)
    p_XY=reshape(p_XY',1,4);

    [freq, counts] = bell_sample(N, p_XY, prob);
end
