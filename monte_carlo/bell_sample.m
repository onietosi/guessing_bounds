function [freq, counts] = bell_sample(N, p_XY, prob)
% SIMULATE Sample N rounds of a Bell experiment.
%    [F,C] = SIMULATE(N,p_XY,prob) simulates N rounds of measurements with input
%    distribution p_XY and behaviour prob. It returns event frequencies F and
%    counts C, ordered as in prob.

    % Maximum size of arrays used in vectorised calculations to generate MC
    % data (limited by internal memory)
    n_max=1E8;

    % Declare arrays to hold joint data (a,b,x,y) after n rounds
    counts = zeros(16,1);

    % SAMPLE INPUTS
    counts_xy = multinomialvariate(N, p_XY, n_max);

    % SAMPLE OUTPUTS
    for j=1:4;
        k=4*(j-1)+1;
        counts(k:k+3) = multinomialvariate(counts_xy(j), prob(k:k+3), n_max);
    end

    % CALCULATE FREQUENCIES (16-component column vector)
    p_XY_long = reshape(repmat(p_XY,4,1),16,1);
    freq=counts./(p_XY_long*N);
end
