function [ counts ] = multinomialvariate(n, p, chunk_size)
% MULTINOMIALVARIATE  Multinomially distributed histogram.
%   H = MULTINOMIALVARIATE(N, P, chunk_size) returns a histogram sampled
%   from the given multinomial distribution of parameter N and probability
%   weights given by vector P.
%
%   Depending on N, one of two algorithms is used. For small N the sampling
%   is done using Monte Carlo simulation of N samples of the discrete
%   distribution. For large N the sampling is done by approximating
%   binomial distributions using normal distributions with a cutoff for
%   out-of-range values.
%
%   chunk_size is the amount of random doubles to generate in one iteration
%   when sampling binomials in the Monte Carlo algorithm. Adjust so that it
%   fits memory.

    %Conditions that might otherwise result in endless loops
    assert(all(p >= 0));
    assert(abs(sum(p(:)) - 1) <= 1e-6);
    assert(n >= 0);

    np = numel(p);
    [p, index] = sort(p(:), 1, 'descend');
    counts = zeros(np, 1);

    pos = np;
    while pos >= 2
        if n <= 0
            return;
        end

        %The pos-th variable is binomial. Sample it.
        counts(pos) = binomialvariate(n, p(pos), chunk_size);
        n = n - counts(pos);
        pos = pos - 1;

        %Condition distribution of remaining variables on x != np.
        p(1:pos) = p(1:pos) / sum(p(1:pos));
    end
    counts(1) = n;

    %Undo the sorting of events.
    counts(index) = counts;
end

function [m] = binomialvariate(n,p,chunk_size)
    nminp = n*min(p,1-p);
    if n >= 1e4 && (nminp >= 20 || n >= 1e7)
        if nminp <= 20
            %n is larger than 1e7 yet n*p or n*(1-p) has remained small
            warning(['n*p or n*(1-p) is quite small, the multinomial ' ...
            'distribution might not be accurately sampled!']);
        end
        m = binomialvariate_fast(n,p);
    else
        %We try to avoid small n, n*p or n*(1-p), at the cost of slowness.
        m = binomialvariate_slow(n,p,chunk_size);
    end
end

function [m] = binomialvariate_slow(n,p,chunk_size)
% BINOMIALVARIATE_SLOW  Binomially distributed random number.
    m = 0;
    while n > 0
        nr = min(n, chunk_size);
        m = m + sum(rand(nr,1) < p);
        n = n - chunk_size;
    end
end

function [m] = binomialvariate_fast(n,p)
% BINOMIALVARIATE_FAST  Approximative binomially distributed random number.
    mu = n*p;
    sigma = sqrt(n*p*(1-p));
    while 1
        m = round(randn(1)*sigma + mu);
        if 0 <= m && m <= n
            break;
        end
        %This will almost never loop, unless n*p is of the order of 1.
    end
end
